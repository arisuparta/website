<?php

if (isset($_POST["submit"])) {
    $nama = $_POST["nama"]; //mengambil inputan yang telah dimasukkan dan mendefinisikannya menjadi $nama
    $mapel = $_POST["mapel"]; //mengambil inputan yang telah dimasukkan dan mendefinisikannya menjadi $mapel
    $uts = $_POST["UTS"]; //mengambil inputan yang telah dimasukkan dan mendefinisikannya menjadi $UTS
    $uas = $_POST["UAS"]; //mengambil inputan yang telah dimasukkan dan mendefinisikannya menjadi $UAS
    $tugas = $_POST["tugas"]; //mengambil inputan yang telah dimasukkan dan mendefinisikannya menjadi $tugas

    //mencari nilai akhir dan menghitung nilai akhir
    $nilaiuts = $uts  * 0.35;
    $nilaiuas = $uas * 0.5;
    $nilaitugas = $tugas * 0.15;

    $total = $nilaiuts + $nilaiuas + $nilaitugas;

    //Let's Grading
    if ($total >= 90 && $total <= 100) {
        $grade = "A";
    } else if ($total > 70 && $total < 90) {
        $grade = "B";
    } else if ($total > 50 && $total <= 70) {
        $grade = "C";
    } else if ($total <= 50) {
        $grade = "D";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Data Nilai</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="container p-3 my-3 bg-success text-white ">
                <!-- Mengatur posisi dan warna pada countainer  -->
                <br>

                <h1>Data Siswa</h1>
                <p>Masukkan data nilai siswa</p>
            </div>
            <form action="" method="post">
                <!-- Form nama -->
                <div class="form1">
                    <label for="Nama">Nama :</label><br>
                    <input type="text" class="form-control" name="nama" id="nama"><br>
                </div>

                <!-- Form mata kuliah -->
                <div class="form2">
                    <label for="mapel">Mata Pelajaran :</label><br>
                    <select class="form-control" id="mapel" name="mapel">
                        <option value="IPA">IPA</option>
                        <option value="AGAMA">AGAMA</option>
                        <option value="PPKN">PPKN</option>
                        <option value="MATEMATIKA">MATEMATIKA</option>
                    </select><br>
                </div>

                <!-- Form Nilai-Nilai -->
                <div class="form3">
                    <label for="Nilai UTS">Nilai UTS :</label><br>
                    <input type="number" class="form-control" name="UTS" id="2"> <br>
                </div>

                <div class="form3">
                    <label for="Nilai UAS">Nilai UAS :</label><br>
                    <input type="number" class="form-control" name="UAS" id="2"> <br>
                </div>

                <div class="form3">
                    <label for="Nilai Tugas">Nilai Tugas :</label><br>
                    <input type="number" class="form-control" name="tugas" id="3"> <br> <br>
                </div>

                <input type="submit" class="btn btn-success" name="submit" value='submit'>
            </form><br>
        </div>
    </div>
    <!-- Output Process -->
    <?php if (isset($_POST["submit"])) : ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="alert alert-success">
                <?php
                    echo "Nama Siswa :$nama <br>";
                    echo "Nama Mata Pelajaran : $mapel <br>";
                    echo "Nilai UTS : $uts <br>";
                    echo "Nilai UAS : $uas <br>";
                    echo "Nilai Tugas : $tugas <br>";
                    echo "Total Nilai : $total <br>";
                    echo "Grade : $grade";
                    ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</body>

</html>